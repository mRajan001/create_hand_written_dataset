#%%
import cv2, os, random
import numpy as np
from PIL import Image
import pytesseract
# from scipy.ndimage import convolve

#%% 
def inverted_binary(image, kernel_size):
    """A function to create inverted binary image with dilation."""
    kernel = np.ones((kernel_size, kernel_size),np.uint8)
    _, th2 = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    inverse = cv2.bitwise_not(th2) #inverse of thresholded image

    dilated_image = cv2.dilate(inverse,kernel,iterations = 1) #fill the holes and dilate in the mask image
    return dilated_image

image = cv2.imread('covid.jpeg', 0)

cv2.imwrite('test_image.png',inverted_binary(image, 5))
# cv2.imshow("dilated", dilated)
# cv2.waitKey(0)
# cv2.destroyAllWindows()


#%% pytesseract just to see 
pytesseract.pytesseract.tesseract_cmd = r'C:\Users\mitz9\AppData\Local\Tesseract-OCR\tesseract.exe'

text = pytesseract.image_to_string(Image.open('image_inverted.png'))

print(text)

# %%
 
def makedir(directory):
    """Creates a new directory if it does not exist"""
    if not os.path.exists(directory):
        os.makedirs(directory)
        return None, 0

dir_list = ['sigma', 'c', 'o', 'v', 'i', 'd', '-', '1', '9']

#%%
for i in dir_list:# range(0,10):
    directory_name = "./images/"+str(i)
    print(directory_name)
    makedir(directory_name) 

# %%
def DataAugmentation(frame, dim = 32):
    """Randomly alters the image using noise, pixelation and streching image functions"""
    frame = cv2.resize(frame, None, fx=2, fy=2, interpolation = cv2.INTER_CUBIC)
    frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB)
    random_num = np.random.randint(1,4)
 
    if (random_num % 2 == 0):
        frame = add_noise(frame)
        print('Noise')
    if(random_num % 3 == 0):
        frame = pixelate(frame)
        print('Pixelate')
    if(random_num % 2 == 0):
        frame = stretch(frame)
        print('Stretch')
    frame = cv2.resize(frame, (dim, dim), interpolation = cv2.INTER_AREA)
 
    return frame 

def add_noise(image):
    """Addings noise to image"""
    prob = random.uniform(0.01, 0.05)
    rnd = np.random.rand(image.shape[0], image.shape[1])
    noisy = image.copy()
    noisy[rnd < prob] = 0
    noisy[rnd > 1 - prob] = 1
    return noisy

def pixelate(image):
    "Pixelates an image by reducing the resolution then upscaling it"
    dim = np.random.randint(8,12)
    image = cv2.resize(image, (dim, dim), interpolation = cv2.INTER_AREA)
    image = cv2.resize(image, (16, 16), interpolation = cv2.INTER_AREA)
    return image
 
def stretch(image):
    "Randomly applies different degrees of stretch to image"
    ran = np.random.randint(0,3)*2
    if np.random.randint(0,2) == 0:
        frame = cv2.resize(image, (32, ran+32), interpolation = cv2.INTER_AREA)
        return frame[int(ran/2):int(ran+32)-int(ran/2), 0:32]
    else:
        frame = cv2.resize(image, (ran+32, 32), interpolation = cv2.INTER_AREA)
        return frame[0:32, int(ran/2):int(ran+32)-int(ran/2)]
def pre_process(image, inv = False):
    """Uses OTSU binarization on an image"""
    try:
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    except:
        gray_image = image
        pass
    
    if inv == False:
        _, th2 = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    else:
        _, th2 = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    resized = cv2.resize(th2, (64, 64), interpolation = cv2.INTER_AREA)
    return resized

# %%

def obj_cord(mask):
    '''
    This is an easy function to get extreme white pizels. Especially useful in cropping the image. 
    Also to get coordinates of the object to save object location i .json file.
    '''
    x = np.where(mask.sum(axis=0))
    y = np.where(mask.sum(axis=1))

    xmin = int(x[0][0])
    xmax = int(x[0][-1])
    ymin = int(y[0][0])
    ymax = int(y[0][-1])

    return xmin, xmax, ymin, ymax

#%%
xmin, xmax, ymin, ymax = obj_cord(cv2.imread("image_inverted.png"))

#%%

test_image = cv2.imread("image_inverted.jpg", 0)


# This is the coordinates of the region enclosing  the first digit
# This is preset and was done manually based on this specific image
# region = [(36, 50), (170, 190)]
 
# Assigns values to each region for ease of interpretation
top_left_y = region[0][1]
bottom_right_y = region[1][1]
top_left_x = region[0][0] 
bottom_right_x = region[1][0]
 
for i in range(0,1): #We only look at the first digit in testing out augmentation functions
    roi = test_image[top_left_y:bottom_right_y, top_left_x:bottom_right_x]
    for j in range(0,6):
        roi2 = DataAugmentation(roi)
        roi_otsu = pre_process(roi2, inv = False)
        cv2.imshow("otsu", roi_otsu)
        cv2.waitKey(0)
        
cv2.destroyAllWindows()

# %%


# %%
