#%% import dependancies 
import cv2, os, random
import numpy as np
from PIL import Image
import pytesseract

#%% 
def inverted_binary(image, kernel_size):
    """A function to create inverted binary image with dilation."""
    kernel = np.ones((kernel_size, kernel_size),np.uint8)
    _, th2 = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    inverse = cv2.bitwise_not(th2) #inverse of thresholded image

    dilated_image = cv2.dilate(inverse,kernel,iterations = 1) #fill the holes and dilate in the mask image
    return dilated_image

# %% 
def makedir(directory):
    """Creates a new directory if it does not exist"""
    if not os.path.exists(directory):
        os.makedirs(directory)
        return None, 0

#%%
def obj_cord(mask):
    '''
    This is an easy function to get extreme white pixels. Especially useful in cropping the image. 
    Also to get coordinates of the object to save object location in .json file.
    '''
    
    x = np.where(mask.sum(axis=0))
    y = np.where(mask.sum(axis=1))

    xmin = int(x[0][0])
    xmax = int(x[0][-1])
    ymin = int(y[0][0])
    ymax = int(y[0][-1])

    return xmin, xmax, ymin, ymax

#%%
dim = 64
# %%
def DataAugmentation(frame, dim = dim):
    """Randomly alters the image using noise, pixelation and streching image functions"""
    frame = cv2.resize(frame, None, fx=2, fy=2, interpolation = cv2.INTER_CUBIC)
    frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2RGB)
    random_num = np.random.randint(1,5)
 
    if random_num == 1:
        frame = add_noise(frame)

    elif random_num == 2:
        frame = pixelate(frame)

    elif random_num ==3:
        frame = noise_stretch(frame)

    else:
        frame = stretch(frame)
    frame = cv2.resize(frame, (dim, dim), interpolation = cv2.INTER_AREA)
 
    return frame 

def add_noise(image):
    """Addings noise to image"""
    prob = random.uniform(0.01, 0.05)
    rnd = np.random.rand(image.shape[0], image.shape[1])
    noisy = image.copy()
    noisy[rnd < prob] = 0
    noisy[rnd > 1 - prob] = 1
    return noisy

def pixelate(image):
    "Pixelates an image by reducing the resolution then upscaling it"
    rand_dim = np.random.randint(8,12)
    image = cv2.resize(image, (rand_dim, rand_dim), interpolation = cv2.INTER_AREA)
    image = cv2.resize(image, (16, 16), interpolation = cv2.INTER_AREA)
    return image
 
def stretch(image):
    "Randomly applies different degrees of stretch to image"
    ran = np.random.randint(0,3)*2
    if np.random.randint(0,2) == 0:
        frame = cv2.resize(image, (dim, ran+dim), interpolation = cv2.INTER_AREA)
        return frame[int(ran/2):int(ran+dim)-int(ran/2), 0:dim]
    else:
        frame = cv2.resize(image, (ran+dim, dim), interpolation = cv2.INTER_AREA)
        return frame[0:dim, int(ran/2):int(ran+dim)-int(ran/2)]

def noise_stretch(image):
    noisy = add_noise(image)
    return stretch(noisy)


def pre_process(image, inv = False):
    """Uses OTSU binarization on an image"""
    try:
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    except:
        gray_image = image
        pass
    
    if inv == False:
        _, th2 = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    else:
        _, th2 = cv2.threshold(gray_image, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    resized = cv2.resize(th2, (dim, dim), interpolation = cv2.INTER_AREA)
    return resized

#%%
