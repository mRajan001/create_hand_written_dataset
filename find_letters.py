#%%
import cv2
import numpy as np
from utils_main import *

# %% load the image
image = cv2.imread('python2.jpeg', 0)
cv2.imwrite('python2.png',inverted_binary(image, 5))

#%%
image = cv2.imread("python2.png") 

original = image.copy()
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
blurred = cv2.GaussianBlur(gray, (3, 3), 0)
canny = cv2.Canny(blurred, 120, 255, 1)
kernel = np.ones((5,5),np.uint8)
dilate = cv2.dilate(canny, kernel, iterations=1)

# Find contours 
cnts = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = cnts[0] if len(cnts) == 2 else cnts[1]

#%% Iterate thorugh contours and filter for ROI 
image_number = 0
for c in cnts:
    x,y,w,h = cv2.boundingRect(c)
    cv2.rectangle(image, (x, y), (x + w, y + h), (0,255,0), 2)
    ROI = original[y:y+h, x:x+w]
    cv2.imwrite("roi/ROI_{}.png".format(image_number), ROI)
    image_number += 1

#uncomment to visualise images
# cv2.imshow('canny', canny)
# cv2.imshow('image', image)
cv2.waitKey(0)
cv2.destroyAllWindows()
