#%% import dependancies
import cv2, os, random, pytesseract
import numpy as np
from PIL import Image
from utils_main import *
from ref import *

# %% using pytesseract to find the letters from the image and create folders with "letters" name
pytesseract.pytesseract.tesseract_cmd = pytesseract_loc
text = pytesseract.image_to_string(Image.open('python2.png'))
print(text)

# %%
dir_list = [i for i in text] 
# %%
for i in dir_list:
    directory_name = "./images/"+str(i)
    makedir(directory_name)         

# %% testing with a single image
test_image = cv2.imread("roi\ROI_5.png", 0)
output_dir = './images/P/'

for j in range(5):
    roi2 = DataAugmentation(test_image)
    roi_otsu = pre_process(roi2, inv = False)
    cv2.imwrite(output_dir +'P_00_'+str(j)+'.png', roi_otsu)
    # cv2.imshow("otsu", roi_otsu) #uncomment to visualization
    cv2.waitKey(0)
        
cv2.destroyAllWindows()
