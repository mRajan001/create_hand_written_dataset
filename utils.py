#%% import dependancies 
import cv2, os, random
import numpy as np
from PIL import Image
import pytesseract

# %% preprocesss

def inverted_binary(image, kernel_size):
    """A function to create inverted binary image with dilation."""
    kernel = np.ones((kernel_size, kernel_size),np.uint8)
    _, th2 = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    inverse = cv2.bitwise_not(th2) #inverse of thresholded image

    dilated_image = cv2.dilate(inverse,kernel,iterations = 1) #fill the holes and dilate in the mask image
    return dilated_image

def makedir(directory):
    """Creates a new directory if it does not exist"""
    if not os.path.exists(directory):
        os.makedirs(directory)
        return None, 0

def obj_cord(mask):
    '''
    This is an easy function to get extreme white pixels. Especially useful to crop the image. 
    Also to get coordinates of the object to save object location in .json file.
    '''
    x = np.where(mask.sum(axis=0))
    y = np.where(mask.sum(axis=1))

    xmin = int(x[0][0])
    xmax = int(x[0][-1])
    ymin = int(y[0][0])
    ymax = int(y[0][-1])

    return xmin, xmax, ymin, ymax
#%%
class DataAugmentation:
    """
    DataAugmentation class to do manual augmentation in the image.
    Such as add noise, pixelate and stretch the image.
    """

    def __init__(self, fname, dim):
        """Randomly alters the image using noise, pixelation and streching image functions"""
        self.frame = cv2.resize(self.frame, None, fx=2, fy=2, interpolation = cv2.INTER_CUBIC)
        self.frame = cv2.cvtColor(self.frame, cv2.COLOR_GRAY2RGB)
        random_num = np.random.randint(1,5)
    
        if random_num == 1:
            self.frame = add_noise(self.frame)

        elif random_num == 2:
            self.frame = pixelate(self.frame)

        elif random_num ==3:
            self.frame = noise_stretch(self.frame)

        else:
            self.frame = stretch(self.frame)
        self.frame = cv2.resize(self.frame, (self.dim, self.dim), interpolation = cv2.INTER_AREA)
    
        return self.frame 

    def add_noise(image):
        """Addings noise to image"""
    prob = random.uniform(0.01, 0.05)
    rnd = np.random.rand(image.shape[0], image.shape[1])
    noisy = image.copy()
    noisy[rnd < prob] = 0
    noisy[rnd > 1 - prob] = 1
    return noisy

def pixelate(image):
    "Pixelates an image by reducing the resolution then upscaling it"
    rand_dim = np.random.randint(8,12)
    image = cv2.resize(image, (rand_dim, rand_dim), interpolation = cv2.INTER_AREA)
    image = cv2.resize(image, (16, 16), interpolation = cv2.INTER_AREA)
    return image
 
def stretch(image):
    "Randomly applies different degrees of stretch to image"
    ran = np.random.randint(0,3)*2
    if np.random.randint(0,2) == 0:
        frame = cv2.resize(image, (dim, ran+dim), interpolation = cv2.INTER_AREA)
        return frame[int(ran/2):int(ran+dim)-int(ran/2), 0:dim]
    else:
        frame = cv2.resize(image, (ran+dim, dim), interpolation = cv2.INTER_AREA)
        return frame[0:dim, int(ran/2):int(ran+dim)-int(ran/2)]


# %%
